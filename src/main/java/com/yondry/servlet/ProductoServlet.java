package com.yondry.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.NumberFormat;
import java.text.spi.NumberFormatProvider;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.persistence.internal.expressions.CompoundExpression;
import org.mariadb.jdbc.internal.packet.dao.parameters.NullParameter;
import org.omg.CORBA.Request;
import com.yondry.entidad.Producto;
public class ProductoServlet extends HttpServlet{

	
	private static final long serialVersionUID = 3601957184140093784L;

	//metodo que valida que solo se inserten digitos
	public boolean esUnNumero(String s){
		boolean verf = true;
		for(int i=0; i < s.length(); i++){
			if(!Character.isDigit(s.charAt(i))){
				verf = false;
			}else{
				verf = true;
			}
		}
	return verf;	
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	EntityManagerFactory emf = Persistence.createEntityManagerFactory("persistence");
	EntityManager em = emf.createEntityManager();
	
	PrintWriter out = resp.getWriter();
	
	//lista de productos
	List <Producto> lista = new ArrayList<Producto>();
	String datos = req.getParameter("datos");
	
	//dato boolean para confirmar si el dato es null
	System.out.println("datos es igual a: "+datos);
	ProductoServlet ps = new ProductoServlet();
	
	//try-catch
	try{
	int dato = Integer.parseInt(datos);
		
		lista = em.createNamedQuery("Producto.buscarPrecio", Producto.class)
			.setParameter("precio",dato)
			.getResultList();
		out.println(lista);
		
	}catch (NumberFormatException nfe) {	
		//condicion que valida si el dato es null
		if(datos == null){	
		lista = em.createNamedQuery("Producto.buscarProductos", Producto.class).getResultList();
		}
		
		//condicion de que hacer si el parametro tiene String
		if(datos != null && esUnNumero(datos) == false){
			System.out.println("entre al if de invalid ");
			req.getRequestDispatcher("/invalid.jsp").forward(req, resp);
			
		}
		
	}
	req.setAttribute("datos", datos);
	req.setAttribute("lista", lista);
	req.getRequestDispatcher("/producto.jsp").forward(req, resp);
	
	out.close();
	}
	
}
